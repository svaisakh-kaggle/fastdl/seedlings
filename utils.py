import subprocess

from fastai.imports import *
from fastai.dataset import *
from fastai.model import *
from fastai.conv_learner import *

from pathlib import Path
from contextlib import contextmanager

shrun = lambda cmd: print(subprocess.getoutput(cmd))

def _make(self):
    self.mkdir(parents=True, exist_ok=True)
    return self
Path.make = _make

COMPETITION = 'plant-seedlings-classification'
DIR_DATA = (Path('~/.kaggle/Competitions').expanduser() / COMPETITION).make()
DIR_CHECKPOINTS = (Path.cwd() / 'Checkpoints').make()

@contextmanager
def workdir(path):
    path_cwd = os.getcwd()
    os.chdir(path)

    try: yield
    finally: os.chdir(path_cwd)

def show_image(img, title=None):
    plt.imshow(img)
    plt.grid(None); plt.xticks([]); plt.yticks([])
    if title is not None: plt.title(title)
    plt.show()

@workdir(DIR_DATA)
def download_and_extract():
    if (DIR_DATA / 'train').exists(): return

    files = ('train.zip', 'test.zip')

    for file in files:
        shrun(f'kaggle competitions download -c {COMPETITION} -f {file}')
        shrun(f'unzip {file} && rm {file}')

    os.mkdir('valid')

def movefiles(path_from, path_to, frac):
    filenames = list(path_from.glob('*'))
    num_files = int(len(filenames) * frac)

    filenames = np.random.choice(filenames, size=num_files, replace=False)

    for filename in tqdm(filenames): os.rename(filename, path_to / filename.name)

def create_validation_set(frac):
    for subdir in DIR_DATA.glob('train/*'):
        path_to = DIR_DATA / 'valid' / subdir.stem
        os.mkdir(path_to)
        movefiles(subdir, path_to, frac)

@workdir(DIR_CHECKPOINTS)
def submit(preds, classes, file_ids, message, filename='submission'):
    y = preds.mean(axis=0).argmax(axis=-1)
    species = np.array(classes)[y]

    submission = {'file': file_ids, 'species': species}
    submission = pd.DataFrame(submission)
    submission.to_csv(DIR_CHECKPOINTS / f'{filename}.csv', index=False)

    shrun(f'gzip {filename}.csv')
    shrun(f'kaggle competitions submit {COMPETITION} -f {filename}.csv.gz -m "{message}"')
    shrun(f'rm {filename}.csv.gz')